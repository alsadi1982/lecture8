<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="style.css">
</head>
<body>
<h1>Информация об авторе</h1>
<p>Фамилия: ${surName}</p>
<p>Имя: ${firstName}</p>
<p>Отчество: ${lastName}</p>
<p>Телефон: ${phone}</p>
<p>Хобби: ${hobbies}</p>
<p>Bitbucket url: <a href="https://bitbucket.org/alsadi1982/" target="_blank">${bitbucketURL}</a></p>
</body>
</html>
