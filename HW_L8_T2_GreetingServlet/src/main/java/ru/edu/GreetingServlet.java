package ru.edu;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GreetingServlet extends HttpServlet {

    private String surName = "Садиков";
    private String firstName = "Александр";
    private String lastName = "Владимирович";
    private String phone = "8-908-729-13 32";
    private String hobbies = "java, english, computer games, reading books";
    private String bitbucketURL = "https://bitbucket.org/alsadi1982/";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("surName", surName);
        req.setAttribute("firstName", firstName);
        req.setAttribute("lastName", lastName);
        req.setAttribute("phone", phone);
        req.setAttribute("hobbies", hobbies);
        req.setAttribute("bitbucketURL", bitbucketURL);


        getServletContext().getRequestDispatcher("/author_page.jsp").forward(req, resp);
    }
}
