package ru.edu;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FinancialServlet extends HttpServlet {

    private String flag = "get";
    private String resultMessage = "";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        if (flag.equals("get")) {
            getServletContext().getRequestDispatcher("/finance.jsp").forward(req, resp);
        }else if (flag.equals("error")){
            getServletContext().getRequestDispatcher("/error.jsp").forward(req, resp);
            flag = "get";
        }else if (flag.equals("result")){
            req.setAttribute("resultMessage", resultMessage);
            getServletContext().getRequestDispatcher("/result.jsp").forward(req, resp);
            flag = "get";
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
           String sum = req.getParameter("sum");
           String percentage = req.getParameter("percentage");
           String years = req.getParameter("years");
           long result;
        try {
          double sumDigit = Double.parseDouble(sum);
          double percentageDigit = Double.parseDouble(percentage);
          double yearsDigit = Double.parseDouble(years);

            if (sumDigit < 50000){
                flag = "error";
            }else {
                flag = "result";
                result = Math.round((sumDigit / 100 * percentageDigit) * yearsDigit + sumDigit);
                resultMessage = "Итоговая сумма " + result + " рублей";
            }
       }catch(Exception ex){
           flag = "result";
           resultMessage = "Неверный формат данных.\nСкорректируйте значения";
       }

        resp.sendRedirect("finance");

    }
}
