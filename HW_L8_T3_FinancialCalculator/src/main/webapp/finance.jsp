<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="style.css">
</head>
<body>
<h1>Калькулятор доходности вклада</h1>
<form method="POST">
<table>
    <tr>
        <td>Сумма на момент открытия</td>
        <td><input class="value" type="text" name="sum"></td>
    </tr>
    <tr>
        <td>Процентная ставка</td>
        <td><input class="value" type="text" name="percentage"></td>
    </tr>
    <tr>
        <td>Количество лет</td>
        <td><input class="value" type="text" name="years"></td>
    </tr>
</table>
<input id="button" type="submit" value="Посчитать" />
</form>
</body>
</html>
